import json
import pika
import django
import os
import sys
import time
from pika.exceptions import AMQPConnectionError
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    data = json.loads(body)
    presenter_name = data["presenter_name"]
    presenter_email = data["presenter_email"]
    title = data["title"]
    send_mail(
        "Your presentation is valid",
        f"{presenter_name}, we are happy to inform you that your presentation {title} has been accepted.",
        "admin@conference.go",
        [f"{presenter_email}"],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    data = json.loads(body)
    presenter_name = data["presenter_name"]
    presenter_email = data["presenter_email"]
    title = data["title"]
    send_mail(
        "Your presentation has been rejected",
        f"{presenter_name}, we are happy to inform you that your presentation {title} has been accepted.",
        "admin@conference.go",
        [f"{presenter_email}"],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
