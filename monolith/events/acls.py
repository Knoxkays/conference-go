import requests
import json

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # USE THE PEXEL KEY
    header = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city} {state}"
    response = requests.get(
        url,
        headers=header,
    )
    content = response.json()
    return content["photos"][0]["src"]["medium"]


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geocoding_url, params=params)
    content = response.json()

    try:
        latitude = content[0]["lat"]
        long = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": latitude,
        "lon": long,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(weather_url, params=weather_params)
    content = response.json()
    try:
        weather = content["weather"][0]["description"]
        temperature = content["main"]["temp"]
    except (KeyError, IndexError):
        return None
    return {"description": weather, "temperature": temperature}
